import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class TestAssignment07ex01Student {
	/**
	 * ************************************
	 * Test In-Place HeapSort based on MinHeap implementation
	 * <p>
	 * ************************************
	 */
	//Integer testArray[] = {3, 9, 17, 2, 23, 1, 5, 4, 19, 17, 7, 18, 8, 67, 6, 11, 0};
	private final Integer[] testArray = {4,3,22,10,0};
	private final Integer[] testArray2= {0,1,2,3,4};
	MinHeap heap;

	@Test
	public void testBottomUpHeapConstruction() {
		try {
			heap = new MinHeap(null);
		} catch (Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
		}
		heap = new MinHeap(testArray);
		MinHeap.print();

		assertEquals(testArray.length, MinHeap.size());

		assertEquals(Integer.valueOf(0), heap.removeMin());
		assertEquals(Integer.valueOf(3), heap.removeMin());
		assertEquals(Integer.valueOf(4), heap.removeMin());
		assertEquals(Integer.valueOf(10), heap.removeMin());
		assertEquals(Integer.valueOf(22), heap.removeMin());

		assertEquals(0, MinHeap.size());
		assertEquals(true, MinHeap.isEmpty());
	}

	@Test
	public void testContains() {
		heap = new MinHeap(testArray);
		assertTrue(heap.contains(Integer.valueOf(0)));
		assertTrue(heap.contains(Integer.valueOf(3)));
		assertTrue(heap.contains(Integer.valueOf(4)));
		assertTrue(heap.contains(Integer.valueOf(10)));
		assertTrue(heap.contains(Integer.valueOf(22)));

		assertFalse(heap.contains(Integer.valueOf(44)));

		// ...
		// ...
	}

	@Test
	public void testSort() {
		//heap = new MinHeap(testArray2);
		MinHeap.sort(testArray2);
		MinHeap.print();
		assertEquals(Integer.valueOf(0), testArray2[4]);
		assertEquals(Integer.valueOf(1), testArray2[3]);
		assertEquals(Integer.valueOf(2), testArray2[2]);
		assertEquals(Integer.valueOf(3), testArray2[1]);
		assertEquals(Integer.valueOf(4), testArray2[0]);

	}
}
