import java.util.NoSuchElementException;

public interface MyPriorityQueue {
	
	/** For testing purpose only.
	 * 
	 * @return returns the Heap array.
	 */
	public Integer[] getHeap();
		
	/**
	 * 
	 * @return the current size of the PQ.
	 */
	public int size();
	
	/**
	 * This method removes and returns the minimum element from the PQ. 
	 * 
	 * @return the minimum element of the PQ.
	 * @throws NoSuchElementException if the PQ is empty.
	 */
	public Integer removeMin() throws NoSuchElementException;
	
	/**
	 * Checks if the element val is already stored.
	 * 
	 * @param val is the value to be searched
	 * @return true if val is found, false otherwise.
	 * @throws IllegalArgumentException if val is null.
	 */
	public boolean contains(Integer val) throws IllegalArgumentException;
}
