import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.stream.IntStream;

public class MinHeap {

	// heap storage
	private static Integer[] heap;
	private static int size;

	// For testing purpose only.
	public Integer[] getHeap() {
		return heap;
	}

	/**
	 * Use this constructor to create the MinHeap bottom-up and in-place.
	 *
	 * @throws IllegalArgumentException if list is null;
	 */
	public MinHeap(Integer[] list) throws IllegalArgumentException {
		if (list != null) {
			size = list.length;
			heap = list;
			makeHeap(list);
		} else {
			throw new IllegalArgumentException("Given heap must not be empty!");
		}
	}

	// For testing purpose only
	public static int size() {
		return size;
	}

	/**
	 * @return True if the PQ is empty, false otherwise.
	 */
	public static boolean isEmpty() {
		return size() <= 0;
	}

	/**
	 * This method sorts the given Integer list[] in-place using the constructor MinHeap(Integer list[]).
	 *
	 * @param list contains the elements to be sorted in-place.
	 * @throws IllegalArgumentException if list is null.
	 */
	public static void makeHeap(Integer[] list) throws IllegalArgumentException {
		int arrLength = size - 1;
		for (int i = arrLength; i >= 0; i--) {
			upHeap(i);
		}
		heap = list;
	}

	// simple Sort
	public static void sort(Integer[] list) throws IllegalArgumentException {
		final MinHeap heap = new MinHeap(list);
		IntStream.iterate(list.length - 1, i -> i >= 0, i -> i - 1).forEach(i -> list[i] = heap.removeMin());
	}

	// Compares current leaf & parent --> swaps if  parent > leaf
	private static void upHeap(int index) {
		int curr = heap[index];
		int parentIdx = getParent(index);
		if (parentIdx >= 0) {
			if (curr < heap[parentIdx]) {
				swap(index, parentIdx);
				upHeap(parentIdx);
			}
		}
	}

	/**
	 * This method removes and returns the minimum element from the PQ.
	 *
	 * @return the minimum element of the PQ.
	 * @throws NoSuchElementException if the PQ is empty.
	 */
	public Integer removeMin() throws NoSuchElementException {
		Integer removed = heap[0];        // this throw exception if empty.
		swap(0, size - 1);
		size--;
		if (size > 0) {
			makeHeap(heap);
		}
		return removed;
	}

	public static void print() {
		System.out.println(Arrays.toString(heap) + "HeapSize: " + size());
	}

	// Private Methods

	/**
	 * Checks if the element val is already stored.
	 *
	 * @param val is the value to be searched
	 * @return true if val is found, false otherwise.
	 * @throws IllegalArgumentException if val is null.
	 */
	public boolean contains(Integer val) throws IllegalArgumentException {
		if (val != null) {
			if (heap[0] > val) {
				return false;
			}
			for (int i = 0; i < size; i++) {
				if (heap[i].equals(val)) {
					return true;
				}
			}
			return false;

		} else {
			throw new IllegalArgumentException("Interger value must not be null!");
		}
	}

	// returns the parent index of given heap-node
	private static int getParent(int index) {
		int parent = (index - 1) / 2;
		return parent;
	}

	//changes position of two given array positions
	private static void swap(int index1, int index2) {
		Integer swapInt = heap[index1];
		heap[index1] = heap[index2];
		heap[index2] = swapInt;
	}
}
